const prodConfig = {
    baseUrl: 'https://cloud.10levels.ru/api/crm/'
};
const devConfig = {
    baseUrl: 'http://localhost:3000/api/crm/'
};

if (process.env.NODE_ENV === 'production') {
    module.exports = prodConfig;
} else {
    module.exports = devConfig;
}
