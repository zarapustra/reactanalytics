export default function reducer(
  state = {
      data: {},
      fetching: false,
      error: null
  },
  action
) {
    switch (action.type) {
        case 'WEEK_SAVE_TO_STORE': {
            const { name, value } = action.payload;
            return {
                ...state,
                [name]: value
            };
        }
        case 'FETCH_WEEKS': {
            return { ...state, fetching: true };
        }
        case 'FETCH_WEEKS_REJECTED': {
            return {
                ...state,
                fetching: false,
                error: action.payload
            };
        }
        case 'FETCH_WEEKS_FULFILLED': {
            return {
                ...state,
                fetching: false,
                data: action.payload
            };
        }
        case 'UPDATE_WEEK': {
            const { id, advCost } = action.payload;
            return {
                ...state,
                fetching: true,
                id,
                advCost
            };
        }
        case 'UPDATE_WEEK_REJECTED': {
            return {
                ...state,
                fetching: false,
                error: action.payload
            };
        }
        case 'UPDATE_WEEK_FULFILLED': {
            const weeksObj = action.payload;
            const week = weeksObj[Object.keys(weeksObj)[0]];
            const oldWeeks = state.data;
            const newWeeks = Object.assign({}, oldWeeks, {[week.id]: week});
            return {
                ...state,
                updating: false,
                updated: true,
                data: newWeeks
            };
        }
        default:
            return state;
    }
}
