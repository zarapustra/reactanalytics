export default function reducer(
  state = {
      rawData: [],
      data: {},
      chosenGroup: '',
      fetching: false,
      error: null
  },
  action
) {
    switch (action.type) {
        case 'USER_SAVE_TO_STORE': {
            const { name, value } = action.payload;
            return {
                ...state,
                [name]: value
            };
        }
        case 'FETCH_USERS': {
            return { ...state, fetching: true };
        }
        case 'FETCH_USERS_REJECTED': {
            return {
                ...state,
                fetching: false,
                error: action.payload
            };
        }
        case 'FETCH_USERS_FULFILLED': {
            return {
                ...state,
                fetching: false,
                rawData: action.payload
            };
        }
        case 'SORT_USERS_FULFILLED': {
            return {
                ...state,
                data: action.payload
            };
        }
        case 'CHOOSE_GROUP_OF_USERS_TO_SHOW': {
            return {
                ...state,
                chosenGroup: action.payload
            };
        }
        default:
            return state;
    }
}
