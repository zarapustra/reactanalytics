import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import weeks from './weeksReducer';
import users from './usersReducer';
import auth from './authReducer';
// import * as types from '../actions/types';

const rootReducer = combineReducers({
    auth,
    users,
    weeks,
    routing
});

export default rootReducer;
