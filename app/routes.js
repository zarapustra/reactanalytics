import React from 'react';
import { Route, IndexRoute } from 'react-router';
import UsersContainer from './containers/UsersContainer';
import WeeksContainer from './containers/WeeksContainer';
import App from './components/App';
import Login from './components/Login';
import Logout from './components/Logout';
import { Authenticated } from './wrappers';

export default (
  <Route path="/" component={App}>
    <Route path="login" component={Login} />
    <Route component={Authenticated}>
      <IndexRoute component={WeeksContainer} />
      <Route path="users" component={UsersContainer} />
      <Route path="logout" component={Logout} />
    </Route>
  </Route>
);
