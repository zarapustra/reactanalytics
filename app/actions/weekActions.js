import axios from 'axios';
import { baseUrl } from '../config.js';
const URL = baseUrl + 'weeks';
import { logoutUser } from './authActions';
import ls from 'store';
const token = ls.get('token');

export function saveToStore(name, value) {
    return {
        type: 'WEEK_SAVE_TO_STORE',
        payload: { name, value }
    };
}

export function fetchWeeks(since = null, till = null) {
    const conf = {
        url: URL,
        headers: {
            Authorization: `Token token=${token}`
        },
        params: {
            since: since,
            till: till
        }
    };

    return dispatch => {
        dispatch({ type: 'FETCH_WEEKS' });
        axios
      .request(conf)
      .then(response => {
          dispatch({ type: 'FETCH_WEEKS_FULFILLED', payload: response.data });
      })
      .catch(err => {
          if (err.response && err.response.status === 401) {
              dispatch(logoutUser());
          }
          dispatch({ type: 'FETCH_WEEKS_REJECTED', payload: err });
      });
    };
}

export function updateWeek(id, value) {
    const conf = {
        url: URL,
        method: 'post',
        headers: {
            Authorization: `Token token=${token}`
        },
        data: {
            week_id: id,
            adv_cost: value
        }
    };

    return dispatch => {
        axios
      .request(conf)
      .then(response => {
          dispatch({ type: 'UPDATE_WEEK_FULFILLED', payload: response.data });
      })
      .catch(err => {
          if (err.response && err.response.status === 401) {
              dispatch(logoutUser());
          }
          dispatch({ type: 'UPDATE_WEEK_REJECTED', payload: err });
      });
    };
}
